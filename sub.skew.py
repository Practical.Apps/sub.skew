import re

#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:].lower() != ".srt":
        print("Not a .srt file")
    else:
        try:
            f = open(tf)
            print("File accepted")
            valid=True
        except IOError:
            print("Not a valid file name / file not in this directory")

def stringtotup(rawstr):
    listoff = rawstr.split(sep=':')
    secsplit = listoff[2].split(',')
    return (int(listoff[0]),int(listoff[1]),int(secsplit[0]),int(secsplit[1]))

#Get time offset
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset time in the form AB:CD:EF,HIJ")
    toff=input("Offset: ")
    if re.match('[0-9][0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]', toff):
        tupoff = stringtotup(toff)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time offset direction
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset time direction")
    print("D, d, or + for delayed : H, h, or - for hastened")
    accans = ["D","d","+","H","h","-"]
    tdir=input("Direction: ")
    if tdir in accans:
        if accans.index(tdir) < 3:
            toffdir = "+"
        else:
            toffdir = "-"
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time start
valid=False
while valid == False:
    #Test flag entry
    print("Enter a start time in the form AB:CD:EF,HIJ")
    print("Lines after this time will be offset")
    print("Enter 0 for BOF if exact time is unknown")
    tstart=input("Start: ")
    if tstart == '0':
        tupstart = (0,0,0,0)
        valid = True
    elif re.match('[0-9][0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]', tstart):
        tupstart = stringtotup(tstart)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time end
valid=False
while valid == False:
    #Test flag entry
    print("Enter an end time in the form AB:CD:EF,HIJ")
    print("Subtitles after this time will not be offset")
    print("Enter 9 for EOF if exact time is unknown")
    tend=input("End: ")
    if tend == '9':
        tupend = (99,59,59,999)
        valid = True
    elif re.match('[0-9][0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]', tend):
        tupend = stringtotup(tend)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

def checktime(time,tstart,tend):
    t = time[3] + (time[2]*1000) + (time[1]*100000) + (time[0]*10000000)
    ts = tstart[3] + (tstart[2]*1000) + (tstart[1]*100000) + (tstart[0]*10000000)
    te = tend[3] + (tend[2]*1000) + (tend[1]*100000) + (tend[0]*10000000)
    if t >= ts and t <= te:
        return True
    else:
        return False

def offset(time,os,odir):
    nt = [time[0],time[1],time[2],time[3]]
    if odir == '+':
        odm = -1
    else:
        odm = 1
    if os[0] > 0:
        nt[0] = time[0] - (odm * os[0])
    if os[1] > 0:
        nt[1] = time[1] - (odm * os[1])
        if nt[1] < 0 or nt[1] > 60:
            nt[1] = nt[1] + (odm * 60)
            nt[0] = nt[0] - (odm * 1)
    if os[2] > 0:
        nt[2] = time[2] - (odm * os[2])
        if nt[2] < 0 or nt[2] > 60:
            nt[2] = nt[2] + (odm * 60)
            nt[1] = nt[1] - (odm * 1)
    if os[3] > 0:
        nt[3] = time[3] - (odm * os[3])
        if nt[3] < 0 or nt[3] > 1000:
            nt[3] = nt[3] + (odm * 1000)
            nt[2] = nt[2] - (odm * 1)

    ns = [str(nt[0]),str(nt[1]),str(nt[2]),str(nt[3])]
    if nt[0] < 10:
        ns[0] = ("0" + str(nt[0]))
    if nt[1] < 10:
        ns[1] = ("0" + str(nt[1]))
    if nt[2] < 10:
        ns[2] = ("0" + str(nt[2]))
    if nt[3] < 100:
        ns[3] = ("0" + str(nt[3]))
    if nt[3] < 10:
        ns[3] = ("00" + str(nt[3]))
    retstring = ns[0] + ":" + ns[1] + ":" + ns[2] + "," + ns[3]
    return retstring

newlines = []

#List building
with open(tf) as f:
    for line in f:
        if re.match('[0-9][0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9]', line):
            linesplit = line.split(sep=' --> ')
            linenums = (stringtotup(linesplit[0]),stringtotup(linesplit[1]))
            if checktime(linenums[0],tupstart,tupend):
                newtimes = offset(linenums[0],tupoff,toffdir) + ' --> ' + offset(linenums[1],tupoff,toffdir) + '\n'
                newlines.append(newtimes)
            else:
                newlines.append(line)
        else:
            newlines.append(line)

#Close file
f.close()

#Name new file
nfilename = tf[:-4] + "_new.srt"

#Open new file
f = open(nfilename,"w+")

#Write to file
for line in newlines:
    f.write(line)

print('Successfully saved to file')

#Close file
f.close()
