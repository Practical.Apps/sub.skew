**Sub.Skew Subtitle timing adjustment tool**

**Summary**\
This python3 script allows you to adjust the time codes on subtitle entries in a .SRT file, shifting the time that each piece of text appears across the whole file or in a specified time frame.
Update: There is now a parallel script that works with .SSA formatted subtitle files. Use `sub.skew.ssa.py` for .SSA files.
Update 2: There is now a new script that stretches/compresses the time codes based on a ratio. Use `sub.stretch.py` if this is what you need.

**Why**\
I recently had the pleasure of watching a hidden gem of 90s television *Acapulco H.E.A.T.*. A show I would recommend to any fan of the era.
There was an unfortunate error in the fifth episode *Code Name: Million Dollar Ladies* which was not only in my copy of the series. It may have been an issue with the broadcast or its conversion to digital form online, but two large parts of the episode had been switched. Two 7-10 minute sections between breaks had their order reversed such that the bad guy is caught and then they are suddenly back to trying to catch him. If I was going to save this show for the future I couldn't leave a mistake this big so I edited the video back into its proper condition. (Contact me if you need a copy)\
Not long after I finished, I realized that not only the video, but the subtitle track also matches the incorrect order. No matter though, I decided to create my own script to shift the timecodes, it seemed like something that could be useful to myself and others. There are some tools for this problem out there already but they tend to be either single service websites or full GUI  MS-Windows programs. While it's unlikely to be as helpful, my particular case required number changing too (In an srt file each subtitle element is timecoded *AND* numbered), so I copied the script and made some tweaks to fix those and included it as well (see sub.num.py). Hopefully these can benefit some other freeware enthusiasts who want to edit their own subtitle files.\
Update: I had another occasion which required a similar modification. While watching the anime *Kaiji: Ultimate Survivor* I noticed that episodes 17 and 18 had subtitles which were desynchronized from the video. I realized that others had commented on the upload about this issue as well, being kind enough to provide the correct offset (10.5 seconds). Interestingly, because of the way SSA subtitles are formatted differently from SRT, not all of the subtitles were out of sync. Some types of subtitles such as the opening and ending title song lyrics were correctly timed. I have provided an option to adjust for this in the new SSA version of the code. This was also useful with episode 25 where only the ending credit song lyrics were out of sync, the rest of the file being perfectly fine. It's a mystery how mistakes like these happen but with some minor adjustments to this code it was an easy fix.\
Update 2: I had yet another reason to make a new script. This time the subtitles were off because of a difference in framerate which means the adjustment is not a fixed value but a ratio.

**How**\
The script works like many of my others, with the interactive shell. You run sub.skew.py in the directory with your subtitle file. You are prompted to supply the following:\
File name (any .SRT or .SSA file),\
Time offset (time in srt/ssa format),\
Offset direction (delay/hasten),\
Offset start (time in srt/ssa format),\
Offset end (time in srt/ssa format),\
Once you have entered all the required information the script processes each line, checking for a timecode, whether it's within the specified window, and then makes an adjustment accordingly. At the end you are returned a new file (old.filename_new.srt) with the adjustments rather than overwriting so the old one can be kept as a backup.
Update 2: In sub.stretch.py the time offset is an adjustment factor. Numbers greater than 1 stretch the time out and numbers between 0 and 1 compress the time.

**Tips**\
In my situation described above the subtitles were embeded in an MP4 file rather than separate or in a container file, so if you are in the same position I'll save you some time.

Regarding SSA timecodes: I was a bit confused to see a single zero in the hour column on the subtitle files I used to test the SSA update. In SRT format and from what I saw of the SSA format from specs discussed online, there are usually two digits. Functionally they are the same, I have tested with a single and double zero and both versions worked, so it is more of a stylistic choice for you. If you would like two zeroes simply undo the changes made to the first (0) time group in the offset function.

To extract the subtitle file:\
ffmpeg -i video.mp4 -vn -an -codec:s:0.X srt video.srt\
Where 'X' is the track number assigned to the subtitles. You can check with:\
ffmpeg -i video.mp4\
[Source](https://linux.overshoot.tv/wiki/extract_subtitles_mp4_file)

To embed the new subtitle file:\
ffmpeg -i infile.mp4 -f srt -i infile.srt -c:v copy -c:a copy -c:s mov_text -metadata:s:s:0 language=XXX outfile.mp4\
Where 'XXX' is the language code associated with the subtitle track, in my case 'Eng'.\
[Source](https://gist.github.com/donly/73f0fdb8f02ed6016c636fc12ee362dc)
