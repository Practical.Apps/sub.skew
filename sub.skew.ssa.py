import re

#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:].lower() != ".ssa":
        print("Not a .ssa file")
    else:
        try:
            f = open(tf)
        except IOError:
            print("Not a valid file name / file not in this directory")
        hasevents = False
        hasformat = False
        hasdialog = False
        for line in f:
            if line.startswith("[Events]"):
                hasevents = True
            elif hasevents and not hasformat and line.startswith("Format:"):
                hasformat = True
                subform = line[8:] # Save format string of column names
            elif not hasdialog and line.startswith("Dialogue:"):
                hasdialog = True
        if hasformat and hasdialog:
            print("File Accepted")
            valid = True
        else:
            print("File lacks 'Format' and/or 'Dialogue' elements")

#Scan File
# Find column numbers for relevant data
formlist = subform.split(', ')
for cat in formlist:
    if cat == "Start":
        startcol = formlist.index(cat)
    elif cat == "End":
        endcol = formlist.index(cat)
    elif cat == "Style":
        stylecol = formlist.index(cat)
# Find different types of subtitle in the file
substyles = []
changesty = []
f.seek(0)  # Reset file cursor so reading lines starts at the BOF again
for line in f:
    if line.startswith("Dialogue:"):
        subsamp = line[10:].split(",")
        if subsamp[stylecol] not in substyles:
            substyles.append(subsamp[stylecol])

# Get styles to change
if len(substyles) > 1:
    print("The following subtitle styles were found, enter 0 to change all")
    print("Otherwise enter a list (e.g. 1,2,3,4) corresponding to the types to be changed")
    for style in substyles:
        print(str(substyles.index(style) + 1) + ": " + style)
    valid=False
    while valid == False:
        stytc=input("Styles to change: ")
        if stytc == "0":
            changesty = substyles
        else:
            selected = stytc.split(',')
            for num in selected:
                try:
                    int(num)
                except ValueError:
                    print("Not a valid time, check formatting")
                    continue
                try:
                    changesty.append(substyles[int(num) - 1])
                    valid = True
                except IndexError:
                    print("Chosen style '" + char + "' does not exist please retry")
                    valid = False

def stringtotup(rawstr):
    listoff = rawstr.split(sep=':')
    secsplit = listoff[2].split('.')
    return (int(listoff[0]),int(listoff[1]),int(secsplit[0]),int(secsplit[1]))

#Get time offset
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset time in the form A:BC:DE.FH")
    toff=input("Offset: ")
    if re.match('[0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9]', toff):
        tupoff = stringtotup(toff)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time offset direction
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset time direction")
    print("D, d, or + for delayed : H, h, or - for hastened")
    accans = ["D","d","+","H","h","-"]
    tdir=input("Direction: ")
    if tdir in accans:
        if accans.index(tdir) < 3:
            toffdir = "+"
        else:
            toffdir = "-"
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time start
valid=False
while valid == False:
    #Test flag entry
    print("Enter a start time in the form A:BC:DE.FH")
    print("Lines after this time will be offset")
    print("Enter 0 for BOF if exact time is unknown")
    tstart=input("Start: ")
    if tstart == '0':
        tupstart = (0,0,0,0)
        valid = True
    elif re.match('[0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9]', tstart):
        tupstart = stringtotup(tstart)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

#Get time end
valid=False
while valid == False:
    #Test flag entry
    print("Enter an end time in the form A:BC:DE.FH")
    print("Subtitles after this time will not be offset")
    print("Enter 9 for EOF if exact time is unknown")
    tend=input("End: ")
    if tend == '9':
        tupend = (9,59,59,99)
        valid = True
    elif re.match('[0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9]', tend):
        tupend = stringtotup(tend)
        valid = True
    else:
        print("Not a valid time, check formatting")
        continue

def checktime(time,tstart,tend):
    t = time[3] + (time[2]*100) + (time[1]*10000) + (time[0]*1000000)
    ts = tstart[3] + (tstart[2]*100) + (tstart[1]*10000) + (tstart[0]*1000000)
    te = tend[3] + (tend[2]*100) + (tend[1]*10000) + (tend[0]*1000000)
    if t >= ts and t <= te:
        return True
    else:
        return False

def offset(time,os,odir):
    nt = [time[0],time[1],time[2],time[3]]
    if odir == '+':
        odm = -1
    else:
        odm = 1
    if os[0] > 0:
        nt[0] = time[0] - (odm * os[0])
    if os[1] > 0:
        nt[1] = time[1] - (odm * os[1])
        if nt[1] < 0 or nt[1] > 60:
            nt[1] = nt[1] + (odm * 60)
            nt[0] = nt[0] - (odm * 1)
    if os[2] > 0:
        nt[2] = time[2] - (odm * os[2])
        if nt[2] < 0 or nt[2] > 60:
            nt[2] = nt[2] + (odm * 60)
            nt[1] = nt[1] - (odm * 1)
    if os[3] > 0:
        nt[3] = time[3] - (odm * os[3])
        if nt[3] < 0 or nt[3] > 100:
            nt[3] = nt[3] + (odm * 100)
            nt[2] = nt[2] - (odm * 1)

    ns = [str(nt[0]),str(nt[1]),str(nt[2]),str(nt[3])]
    if nt[1] < 10:
        ns[1] = ("0" + str(nt[1]))
    if nt[2] < 10:
        ns[2] = ("0" + str(nt[2]))
    if nt[3] < 10:
        ns[3] = ("0" + str(nt[3]))
    retstring = ns[0] + ":" + ns[1] + ":" + ns[2] + "." + ns[3]
    return retstring

newlines = []
f.seek(0)

#List building
with open(tf) as f:
    for line in f:
        if line.startswith("Dialogue:"):
            data = line[10:].split(",")
            if data[stylecol] in changesty:
                linenums = (stringtotup(data[startcol]),stringtotup(data[endcol]))
                if checktime(linenums[0],tupstart,tupend):
                    newtimes = offset(linenums[0],tupoff,toffdir) + ',' + offset(linenums[1],tupoff,toffdir)
                    oldtimes = data[startcol]  + ',' + data[endcol]
                    newline = line.replace(oldtimes, newtimes)
                    newlines.append(newline)
            else:
                newlines.append(line)
        else:
            newlines.append(line)

#Close file
f.close()

#Name new file
nfilename = tf[:-4] + "_new.ssa"

#Open new file
f = open(nfilename,"w+")

#Write to file
for line in newlines:
    f.write(line)

print('Successfully saved to file')

#Close file
f.close()

