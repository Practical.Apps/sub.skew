import re

#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:].lower() != ".srt":
        print("Not a .srt file")
    else:
        try:
            f = open(tf)
            print("File accepted")
            valid=True
        except IOError:
            print("Not a valid file name / file not in this directory")

def stringtotup(rawstr):
    listoff = rawstr.split(sep=':')
    secsplit = listoff[2].split(',')
    return (int(listoff[0]),int(listoff[1]),int(secsplit[0]),int(secsplit[1]))

#Get num offset
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset number")
    toff=input("Offset: ")
    if int(toff) > 0:
        tupoff = int(toff)
        valid = True
    else:
        print("Not a valid number, check formatting")
        continue

#Get num offset direction
valid=False
while valid == False:
    #Test flag entry
    print("Enter an offset direction")
    print("D, d, or + for delayed : H, h, or - for hastened")
    accans = ["D","d","+","H","h","-"]
    tdir=input("Direction: ")
    if tdir in accans:
        if accans.index(tdir) < 3:
            toffdir = "+"
        else:
            toffdir = "-"
        valid = True
    else:
        print("Not a valid number, check formatting")
        continue

#Get num start
valid=False
while valid == False:
    #Test flag entry
    print("Enter a start number")
    print("Lines after this will be offset")
    tstart=input("Start: ")
    if int(tstart) > 0:
        tupstart = int(tstart)
        valid = True
    else:
        print("Not a valid number, check formatting")
        continue

#Get num end
valid=False
while valid == False:
    #Test flag entry
    print("Enter an end number")
    print("Subtitles after this will not be offset")
    tend=input("End: ")
    if int(tend) > 0:
        tupend = int(tend)
        valid = True
    else:
        print("Not a valid number, check formatting")
        continue

def checktime(t,ts,te):
    if t >= ts and t <= te:
        return True
    else:
        return False

def offset(time,os,odir):
    if odir == '+':
        odm = -1
    else:
        odm = 1
    retstring = str(time - (odm * os))
    return retstring

newlines = []

#List building
with open(tf) as f:
    for line in f:
        if re.match('[0-9]', line) and re.match('[0-9][0-9]:', line) == None:
            number = int(line)
            if checktime(number,tupstart,tupend):
                newlines.append(offset(number,tupoff,toffdir) + '\n')
            else:
                newlines.append(line)
        else:
            newlines.append(line)


#Close file
f.close()

#Name new file
nfilename = tf[:-4] + "_new.srt"

#Open new file
f = open(nfilename,"w+")

#Write to file
for line in newlines:
    f.write(line)

print('Successfully saved to file')

#Close file
f.close()
